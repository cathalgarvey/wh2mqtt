# wh2mqtt - Glue Your Webhooks, Projects, and Tools With a Simple Message Queue

by Cathal Garvey, Copyright 2020, licensed under the GNU AGPLv3 or any
later version.

**Note: This is super alpha and is barely tested right now. I built it
  to scratch and itch and it's barely ready to do even that.**

## About

MQTT is a simple message queueing system that has become popular among
home automation, IOT, and sensor-network enthusiasts, and is also used
as a communication back-end by visual programming systems such as
Node-Red, Huginn, and more.

A nice application of message queueing as a glue is that they allow
loose coupling between applications. They can also permit a sort of
firewalling between applications and interfaces that should probably
be kept apart: for example, Node-Red is not something I would like to
expose to the internet, but with the right boundary logic I might be
willing to let a Webhook be accessed _by node-red_ (note the
directionality).

`wh2mqtt` is the glue I need to enable loosely coupled workflows like
this. Other projects like it exist already, but I needed a combination
of a few things to make this useful to me:

* A configurable root topic that all messaged topics must belong to:
  this is a basic security feature absent from most other
  implementations I have seen. Without it, a malicious input can be
  delivered to an MQTT topic other than the intended one(s), possibly
  triggering nasty consequences.
* With respect to the above, caller-specified subtopic, allowing webhooks
  to be set up for various different services. For security, consider
  adding suitably entropic random values to your topic paths to prevent
  injection attacks by attackers who know the master topic/URL and guess
  subtopics for other webhooks.
* Basic pass-through of HTTP bodies (e.g. `--data` parameter of `curl`),
  allowing coupled applications to choose how to parse or use them.
* More complete, structured output to another topic, providing access
  to as much HTTP context as possible.

The above should allow one to use wh2mqtt to set up webhook
endpoints that capture their inputs and dump them directly to MQTT for
processing by..anything. A cron-job, a daemon, a user-activated
script, that's the beauty of using a lightweight message queue like
Mosquitto to handle your inputs. Just.. be careful with your QOS
settings and checking frequency, or your disk usage for Mosquitto
might balloon if your webhook gets hit a lot or a malicious actor
detects it.

## Basic Usage

The defaults are suitable for a basic mosquitto server running on the
same device, and default to posting to topics within a `/wh2mqtt`
heirarchy. The server listens by default on port 8000. You can add a
`Rocket.toml` file to specify all the information that Rocket normally
accepts, but additional keys in this file can be used to control
`wh2mqtt`:

* `mqtt_server` - Set the server IP or hostname.
* `mqtt_port` - Set the port to access MQTT on.
* `mqtt_base_topic` - The base topic to use for all incoming data.

MQTT support is very basic right now and uses an unencrypted
connection: It's assumed it runs on the same device or within a
trusted LAN.

When messages arrive, a JSON object representing the full HTTP context
will be posted to the targeted topic, and the request body will also
be posted to `<topic>/body`. Request body needs to be UTF-8, right
now. That's it.

## Installation

1. Build `wh2mqtt` with `cargo build`, ideally in Release mode when you're ready.
1. Put the resulting `wh2mqtt` binary into `/usr/bin`
1. Put a copy of `wh2mqtt.service` (git root directory) in /etc/systemd/system/
1. (Optional) Put a config file for wh2mqtt at `/usr/local/etc/wh2mqtt/Rocket.toml` (or wherever the systemd unit file declares the working directory to be)
1. `sudo systemctl enable wh2mqtt`
1. `sudo systemctl start wh2mqtt`
1. All things going well, `wh2mqtt` should now launch (and launch at boot) and bind the configured port.

## Practical Usage

Recommended setup is to configure your webserver to host, but not
present for indexing, a long random string path that can't practically
be guessed, and route requests to this URL to wh2mqtt.

Furthermore, when creating webhooks with other services, use a random
string prefix for each webhook so that parties who know the base URL
cannot guess and inject data into other webhooks easily.

### Example

You've set up `wh2mqtt` with your MQTT broker using the default topic,
`wh2mqtt`. Any requests directly to `wh2mqtt` will get dumped into a
topic under `/wh2mqtt`, so a request to `/publish/foo` will appear in
`/wh2mqtt/foo`.

The first layer of security would be to configure the webserver that
is fronting for `wh2mqtt` so that the base URL becomes
`/faoiunvdfiowe23l` from outside, getting redirected to `/publish`
behind the webserver. This prevents anyone who you have not shown a
webhook URL from guessing the base URL and spamming your MQTT
server.

Then, you want to set up a webhook on service_A: You give service_A a
webhook that looks like: `/faoiunvdfiowe23l/waiuhbybxdkjflds`, and the
messages will arrive in your MQTT queue at topic
`/wh2mqtt/waiuhbybxdkjflds`.

It's ugly, but when you later set up a webhook with service_B, and the
admin of service_B is feeling malicious, the worst they can do is spam
random topics under `/wh2mqtt/` because they will be unable to guess
in-use topics.

There are other layers of security you can use, but this basic
best-practice should be considered a core part of using `wh2mqtt` and
is currently left to you to handle (sorry!). Remember that a suitably
random string is important, so ideally you would generate these random
parts of the webhook URL using a password manager's password
generator, sticking to URL-safe characters like numbers and letters.