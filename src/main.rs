#![feature(proc_macro_hygiene, decl_macro)]

#[macro_use] extern crate rocket;
extern crate paho_mqtt as mqtt;
#[macro_use] extern crate serde;
extern crate serde_json;
extern crate base64;

use std::path::PathBuf;

use rocket::{State, Data};

mod serialisablerequest;
mod mqttagent;


fn publish_to_mqtt(mqtt: &mqttagent::MQTTAgent,
                   subtopic: &PathBuf,
                   request: serialisablerequest::H2MRequest,
		   ) -> String {

  if let Some(subtopic_str) = subtopic.clone()
                                      .into_os_string()
				      .to_str() {
    mqtt.publish_request(subtopic_str.into(), request);
    "Published\n".to_string()
  } else {
    mqtt.log(format!("Failed to publish successfully to {}, maybe bad unicode?", subtopic.display()));
    "Failed to publish, maybe bad unicode?\n".to_string()
  }
  
}

#[get("/publish/<subtopic..>?<data>")]
fn publish_get(subtopic: PathBuf,
               data: String,
               request: serialisablerequest::H2MRequest,
               mqtt: State<mqttagent::MQTTAgent>) -> String {
  let request2 = request.with_data(data.clone());
  publish_to_mqtt(&mqtt, &subtopic, request2)
}

#[post("/publish/<subtopic..>", data = "<data>")]
fn publish_post(subtopic: PathBuf,
                data: Data,
		request: serialisablerequest::H2MRequest,
		mqtt: State<mqttagent::MQTTAgent>) -> String {
  let mut buffer = Vec::new();
  let _read_n = data.stream_to(&mut buffer).expect("Failure to read data from request.");

  // If decodeable as UTF-8, post as text, otherwise wrap in base64 and post that.
  let decoded_str = String::from_utf8(buffer.clone());
  let request2 = match decoded_str {
    Ok(read_str) => request.with_data(read_str),
    Err(_) => request.with_bytes(buffer),
  };

  // let request2 = request.with_data(read_str);
  publish_to_mqtt(&mqtt, &subtopic, request2)
}

fn main() {
  let rock = rocket::ignite();

  // TODO: All those other features of MQTT people care about
  let mqtt_server     = rock.config().get_str("mqtt_server").unwrap_or("localhost");
  let mqtt_port       = rock.config().get_str("mqtt_port").unwrap_or("1883");
  let mqtt_base_topic = rock.config().get_str("mqtt_base_topic").unwrap_or("wh2mqtt");

  let mqtt_agent = mqttagent::MQTTAgent::new(
    format!("tcp://{}:{}", mqtt_server, mqtt_port),
    mqtt_base_topic.to_string(),
    format!("{}/{}", mqtt_base_topic, "_manage/log"));

  rock.manage(mqtt_agent)
      .mount("/", routes![publish_get, publish_post])
      .launch();
}

