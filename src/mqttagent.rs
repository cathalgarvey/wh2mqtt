use std::{process};  
use std::time::Duration;

use super::serialisablerequest;

pub struct MQTTAgent {
  client: mqtt::async_client::AsyncClient,
  base_topic: String,
  logging_topic: String,
}

impl MQTTAgent {
  pub fn new(endpoint: String,
         base_topic: String,
         logging_topic: String) -> MQTTAgent {
    let create_opts = mqtt::CreateOptionsBuilder::new()
      .server_uri(endpoint)
      .persistence(mqtt::PersistenceType::None)
      .finalize();

    let cli = mqtt::AsyncClient::new(create_opts).unwrap_or_else(|err| {
      println!("Error creating the MQTT client: {}", err);
      process::exit(1);
    });
    
    // Connect with default options
    let conn_opts = mqtt::ConnectOptions::new();

    // Connect and wait for it to complete or fail
    if let Err(e) = cli.connect(conn_opts).wait_for(Duration::from_secs(3)) {
      println!("Unable to connect to MQTT Endpoint: {:?}", e);
      process::exit(1);
    }

    MQTTAgent {
      client: cli,
      base_topic: base_topic,
      logging_topic: logging_topic,
    }
  }

  fn log_channel(&self) -> String {
    self.logging_topic.clone()
  }

  fn output_topic(&self, subtopic: String) -> String {
    if subtopic == "" {
      format!("{}/{}", self.base_topic, "request")
    } else {
      format!("{}/{}", self.base_topic, subtopic)
    }
  }

  // TODO: in both below methods, need effective error handling please..
  pub fn log(&self, data: String) {
    let msg = mqtt::Message::new(self.log_channel(), data, 0);
    if let Err(e) = self.client.publish(msg).wait_for(Duration::from_secs(3)) {
      println!("Error logging: {}", e);
    }
  }

  fn _publish(&self, msg: mqtt::Message, wait_for: u64) {
    let wd = Duration::from_secs(wait_for);
    if let Err(e) = self.client.publish(msg)
                               .wait_for(wd) {
      let errstring = format!("Error publishing: {}", e);
      println!("{}", errstring);
      self.log(errstring);
    }
  }

  pub fn publish(&self, subtopic: String, data: String) {
    // Where messages get sent to MQTT..
    let msg = mqtt::Message::new(self.output_topic(subtopic),
                                 data,
				 0);
    self._publish(msg, 3);
  }

  pub fn publish_request(&self,
                         subtopic: String,
			 request: serialisablerequest::H2MRequest) {

    let full_context = serde_json::to_string(&request)
                         .expect("Failed to serialise JSON");
    self.publish(subtopic.clone().into(), full_context);
    self.publish(format!("{}/data", subtopic), request.data.unwrap_or("".to_string()));
    self.log(format!("Published successfully to {}", subtopic));
  }
}
