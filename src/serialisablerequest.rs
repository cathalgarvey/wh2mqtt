use std::net::{IpAddr};
use std::collections::{HashMap};

use rocket::request::{FromRequest, Outcome};
use rocket::{Request};

use base64;

#[derive(Serialize, Deserialize, Debug, Clone)]
pub struct H2MRequest {
  pub method: String,
  pub uri: String,
  pub data: Option<String>,  // To be filled in the handler callback
  pub client_ip: Option<IpAddr>,
  pub headers: HashMap<String, Vec<String>>, //TODO

  // TODO: Laters
  //query_parts: map, // TODO
  //route: ??, //TODO
  //cookies: RefCell<CookieJar>, // Make JSONable?

}

impl H2MRequest {
  pub fn with_data(&self, data: String) -> H2MRequest {
    let mut h2m = self.clone();
    h2m.data = Some(data);
    h2m
  }

  pub fn with_bytes(&self, data: Vec<u8>) -> H2MRequest {
    self.with_data(base64::encode(data))
  }

  fn set_header_map(&mut self, request: &Request) {
    
    for header in request.headers().iter() {
      let hname = header.name().to_string();
      if !self.headers.contains_key(&hname) {
        self.headers.insert(hname.clone(), Vec::new());
      }

      self.headers.get_mut(&hname)
                  .expect("Key not found in header_map but should have just been allocated?")
		  .push(header.value().to_string());
    }
  }

}

impl FromRequest<'_, '_> for H2MRequest {
  type Error = String;
  fn from_request(request: &Request) -> Outcome<Self, Self::Error> {
    let uri: String = match request.uri().query() {
      Some(query) => format!("{}?{}", request.uri().path(), query),
      None => request.uri().path().into(),
    };
    
    // Assembly
    let mut h2mr = H2MRequest{
      method: request.method().as_str().into(),
      uri: uri,
      data: Option::None,
      client_ip: request.client_ip(),
      headers: HashMap::new(),
    };
    h2mr.set_header_map(request);
    Outcome::Success(h2mr)
  }
}
